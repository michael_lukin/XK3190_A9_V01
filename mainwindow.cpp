#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(AppData &data, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    update_dlg(data),
    appdata(&data)
{

    work_date = cur_date = QDate::currentDate();
    date_watch_timer.start(60000);

    ui->setupUi(this);

    ui->stackedWidget->setCurrentIndex(0);

    query = std::move(QSqlQuery(appdata->d_base));
    // выборка по текущей дате
    QString querytext = QString(select_cur_date)
    .arg(work_date.toString("dd.MM.yyyy") );
    if(!query.exec(querytext) ){
        QMessageBox::critical(nullptr, "Ошибка",
                              "Не возможно получить данные из base.\n" +
                              query.lastError().text(), QMessageBox::Close);
        exit(0);
    }

    query_model.setQuery(query);

    actibe_table_view = ui->tableView;

    ui->tableView->setModel(&query_model);
    ui->tableView_2->setModel(&query_model);

    query_model.setHeaderData(0, Qt::Horizontal, " Номер ");
    query_model.setHeaderData(1, Qt::Horizontal, "  Дата и время  ");
    query_model.setHeaderData(2, Qt::Horizontal, " Груз ");
    query_model.setHeaderData(3, Qt::Horizontal, " Масса(кг) ");
    query_model.setHeaderData(4, Qt::Horizontal, " Гос. Номер ");
    query_model.setHeaderData(5, Qt::Horizontal, " Тип взвешивания ");
    query_model.setHeaderData(6, Qt::Horizontal, " Контрагент ");
    query_model.setHeaderData(7, Qt::Horizontal, " Инф. ");

    ui->tableView->resizeColumnsToContents();
    ui->tableView->resizeRowsToContents();
    ui->tableView->setColumnHidden(8, true);

    ui->tableView_2->resizeColumnsToContents();
    ui->tableView_2->resizeRowsToContents();
    ui->tableView_2->setColumnHidden(8, true);

    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setPageOrientation(QPageLayout::Landscape);
    printer.setPageMargins(10.0,12.0,10.0,13.0,QPrinter::Millimeter);

    table_menu = new QMenu(this);
    QAction *edit_row = table_menu->addAction("Редактировать запись");
    QAction *remo_row = table_menu->addAction("Удалить запись");

    QObject::connect(edit_row, &QAction::triggered, this,
                     &MainWindow::on_action_edit_cur_record_triggered);
    QObject::connect(remo_row, &QAction::triggered, this,
                     &MainWindow::on_action_remove_cur_record_triggered);

    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableView->setLocale(QLocale::c());
    ui->tableView_2->setLocale(QLocale::c());

    serial.setBaudRate(QSerialPort::Baud9600);
    serial.setDataBits(QSerialPort::Data8);
    serial.setParity(QSerialPort::NoParity);
    serial.setStopBits(QSerialPort::OneStop);
    serial.setFlowControl(QSerialPort::NoFlowControl);

    serial_timer = new QTimer(this);
    serial_timer->setInterval(5000);
    serial_timer->setSingleShot(true);
    on_serial_restart(false);

    connect(&date_watch_timer, &QTimer::timeout, this, &MainWindow::on_dwatch_time_out);
    connect(serial_timer, &QTimer::timeout, this, &MainWindow::on_time_out);
    connect(&serial, &QSerialPort::readyRead, this, &MainWindow::handleReadyRead);
    connect(&serial,
            static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &MainWindow::handleError);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_tableView_doubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index)
    ui->tableView->clearSelection();
}

void MainWindow::on_pushButton_clicked()
{
    // запись показаний в базу
    update_dlg.query_type = UpdateDialog::INSERT;
    update_dlg.data.clear();
    update_dlg.data.weight = cur_weight;
    update_dlg.data.number = appdata->main_counter;
    if(update_dlg.exec() == QDialog::Accepted){
        update_query_model(true);
    }
}

inline void MainWindow::update_query_model(bool main_counter_recal)
{
    // выборка по текущей дате
    QString querytext = QString(select_cur_date)
                        .arg(work_date.toString("dd.MM.yyyy") );
    if(!query.exec(querytext)){
        QMessageBox::critical(nullptr, "Ошибка",
                              "Ошибка запроса на получение данных\n" +
                              query.lastError().text(), QMessageBox::Close);
        /// ВЫВОД В ЛОГ !!!
        exit(0);
    }
    query_model.setQuery(query);
    actibe_table_view->resizeColumnsToContents();
    actibe_table_view->resizeRowsToContents();
    // обновить счетчик взвешиваний по максимальному значению в БАЗЕ
    if(main_counter_recal)
        appdata->main_counter = appdata->max_num_for_cur_date() + 1;
}

void MainWindow::on_action_PDF_triggered()
{
    // выгрузка данных в PDF
    QString path = QFileDialog::getSaveFileName(nullptr, "Экспорт в PDF",
                                                work_date.toString("dd_MM_yyyy"),
                                                "PDF(*.pdf)");
    if(path.isEmpty()) return ;

    ui->stackedWidget->setCurrentIndex(2);
    setDisabled(true);

    printer.setOutputFileName(path);
    QTextDocument doc;
    QString html = "<html><body>";
    html+= R"(<table align = 'center' cellpadding = '5' border = 1 cellspacing = '0' style = 'border-style: solid; font-size: 12pt'>)";
    html+= "<tr style='background-color: #DCDCDC; font-weight: normal;' > <th>Номер</th> <th>Дата и время</th> <th>Груз</th> <th>Масса(кг)</th> <th>Гос. Номер</th> <th>Тип взвешивания</th> <th>Контрагент</th> <th>Инф.</th> </tr>";
    query.first();
    do {
        html+= "<tr>";
        for(int i = 0; i < 8; ++i){
            html+= "<td>";
            html+= query.value(i).toString();
            html+= "</td>";
        }
        html+= "</tr>";
    } while (query.next());

    html += R"(</table></body></html>)";
    doc.setHtml(html);
    doc.setPageSize(printer.pageRect().size()); // This is necessary if you want to hide the page number
    doc.print(&printer);

    setDisabled(false);

    QMessageBox box;
    box.setWindowTitle("Информация");
    box.setText("Файл успешно сохранен!");
    box.exec();

    ui->stackedWidget->setCurrentIndex(work_index_widget);
}

void MainWindow::on_action_PDF_2_triggered()
{
    // расчет превезенного сырья и выгрузка в PDF

    //QString query_text = QString(order_by_regnum).arg(work_date.toString("dd.MM.yyyy") );
    QString query_text = QString(select_for_pdf2).arg(work_date.toString("dd.MM.yyyy") );


    if(!query.exec(query_text)){
        QMessageBox::critical(nullptr, "Ошибка", "Ошибка запроса на получение данных\n" +
                              query.lastError().text(), QMessageBox::Close);
        /// ВЫВОД В ЛОГ !!!
        exit(0);
    }

    QString path = QFileDialog::getSaveFileName(nullptr, "Экспорт в PDF", work_date.toString("dd_MM_yyyy"), "PDF(*.pdf)");
    if(path.isEmpty()) return ;

    ui->stackedWidget->setCurrentIndex(2);
    setDisabled(true);

    printer.setOutputFileName(path);
    QTextDocument doc;
    QString html = "<html><body>";
    html+= R"(<table align = 'center' cellpadding = '5' border = 1 cellspacing = '0' style = 'border-style: solid; font-size: 12pt'>)";
    html+= "<tr style='background-color: #DCDCDC; font-weight: normal;' > <th>Номер</th> <th>Дата и время</th> <th>Груз</th> <th>Масса(кг)</th> <th>Гос. Номер</th> <th>Тип взвешивания</th> <th>Контрагент</th> <th>Инф.</th> </tr>";

    query.first();
    QString prev_num = query.value("reg_num").toString();
    QString prev_typ = query.value("type").toString();
    double  prev_val = query.value("weight").toDouble();

    QString cur_num;
    QString cur_typ;
    double  cur_val = 0.0;

    size_t  rec_count = 0;
    do {
        // добавить текущую запись в отчет
        html+= "<tr>";
        for(int i = 0; i < 8; ++i){
            html+= "<td>";
            html+= query.value(i).toString();
            html+= "</td>";
        }
        html+= "</tr>";

        if(rec_count != 0){
            // выполнить проверку относительно предыдущей записи
            cur_num = query.value(4).toString();
            cur_typ = query.value(5).toString();
            cur_val = query.value(3).toDouble();

//            qDebug() << cur_num << " " << cur_typ << " " << cur_val;

            if(cur_num == prev_num &&
               cur_typ  == "Выездное" &&
               prev_typ == "Въездное")
            {
                double result_value = prev_val - cur_val;
                html+= "<tr>";
                html+= R"(<td colspan = "8">)";
                //html+= "<td>";
                html+= "Итого " + cur_num + " выгрузил: ";
               // html+= "</td>";
                html+= QString::number(result_value);
                html+= "</td>";
                html+= "</tr>";
            }

            prev_num = cur_num;
            prev_typ = cur_typ;
            prev_val = cur_val;
        }

        ++rec_count;
    } while (query.next());

    html += R"(</table></body></html>)";
    doc.setHtml(html);
    doc.setPageSize(printer.pageRect().size()); // This is necessary if you want to hide the page number
    doc.print(&printer);

    setDisabled(false);

    QMessageBox box;
    box.setWindowTitle("Информация");
    box.setText("Файл успешно сохранен!");
    box.exec();

    /// вернуть обычное отоброжение данных в таблице
    update_query_model(false);

    ui->stackedWidget->setCurrentIndex(work_index_widget);
}


void MainWindow::on_action_remove_cur_record_triggered()
{
    int row = ui->tableView->selectionModel()->selectedRows().first().row();
    // вызов диалога редактирования данных взвешивания
    int res = QMessageBox::question(nullptr,"Удаление", "Вы действительно желаете удалить запись о взвешивании \n"
                           "№ " + query_model.index(row, 0).data().toString()+"\n"
                           "выполненное " + query_model.index(row, 1).data().toString());

    if(res == QMessageBox::No) return;
    QSqlQuery delete_query (QSqlQuery(appdata->d_base));
    QString query_text = QString(remove_cur_row)
                         .arg(query_model.index(row, 8).data().toString());

    if(!delete_query.exec(query_text)){
        QMessageBox::critical(nullptr, "Ошибка", "Невозможно выполнить запрос на удаление к dbase\n"+
                              delete_query.lastError().text());
/// ЗАПИСЬ В ЛОГ
        return ;
    }
    update_query_model(true);
}

void MainWindow::on_action_edit_cur_record_triggered()
{
    // обновить значения записи
    int row = ui->tableView->selectionModel()->selectedRows().first().row();

    update_dlg.data.clear();
    update_dlg.data.number   = query_model.index(row, 0).data().toInt();
    update_dlg.data.datetime = QDateTime::fromString(query_model.index(row, 1).data().toString(), "dd.MM.yyyy HH:mm:ss");
    update_dlg.data.cargo    = query_model.index(row, 2).data().toString();
    update_dlg.data.weight   = query_model.index(row, 3).data().toDouble();
    update_dlg.data.regnum   = query_model.index(row, 4).data().toString();
    update_dlg.data.type     = (query_model.index(row, 5).data().toString() == "Въездное") ?
                                Data::Type::IN : Data::Type::OUT;
    update_dlg.data.counteragent    = query_model.index(row, 6).data().toString();
    update_dlg.data.info     = query_model.index(row, 7).data().toString();
    update_dlg.data.rowid    = query_model.index(row, 8).data().toInt();

    // вызов диалога редактирования данных взвешивания
    update_dlg.query_type = UpdateDialog::UPDATE;
    if(update_dlg.exec() == QDialog::Accepted){
        update_query_model(true);
    }
}

void MainWindow::on_tableView_customContextMenuRequested(const QPoint &pos)
{
    if(ui->tableView->selectionModel()->selection().size()>0)
    table_menu->popup(ui->tableView->mapToGlobal(pos));
}

void MainWindow::handleReadyRead()
{
    if(serial_timer->isActive())
        serial_timer->stop();
    else
        return ;

    // считывание полученных данных
    //std::string buf = serial.readAll();
    QByteArray buf = serial.readAll();
    // разбор данных
    for(auto &s : buf){
        if(!begin && (s == 2) ){ // 02 - XON (begin) [не учитывается в count !!!]
            begin   = true;
            dot_num = 0;
            count   = 0;
        }
        else if(s == 3 || count == 11){ // 03 - XOFF (end)
            char number[9];
            memset(number, '\0', 8);
            memcpy(number, res, 7-dot_num);
            if((7-dot_num) < 7)
                memcpy(number+(7-dot_num)+1, res+(7-dot_num), dot_num);
            char *end;
            cur_weight = std::strtod(number, &end); // проверка на число
            ui->label->setText(QString::number(cur_weight));

            begin = false;
            memset(res, 0, 7);

        }
        else{
            if(count < 7) // byte #0 - #6 data (byte #0 - +/-)
                res[count] = s;
            else if(count == 7){
                dot_num = (static_cast<size_t>(s) - 48); // byte #7 - dot position
                dot_num = (dot_num > 4) ? 0 : dot_num;
            }
            ++count;
        }
    }
    if(!serial_timer->isActive()) serial_timer->start(serial_timer_delay);
}

void MainWindow::handleError(QSerialPort::SerialPortError error)
{
   if(serial_timer->isActive()) serial_timer->stop();
    if(error == QSerialPort::NoError ||
       error == QSerialPort::DeviceNotFoundError) // DeviceNotFoundError итак отлавливается в момент выполнения метода open
        return ;
    QMessageBox::critical(nullptr, "Ошибка порта "+serial.portName(),
                          serial.errorString(), QMessageBox::Close);
    on_serial_fall_off("Ошибка порта");
}

void MainWindow::on_time_out()
{
    if(serial_timer->isActive()) serial_timer->stop();
    on_serial_fall_off("НЕТ ДАННЫХ");
    qDebug() << "куку блять";
}

void MainWindow::on_dwatch_time_out()
{
    // проверка смены даты
    if(cur_date != QDate::currentDate()){
        cur_date = QDate::currentDate();
        if(work_index_widget == 0) work_date = cur_date;
        appdata->main_counter = appdata->max_num_for_cur_date() + 1;
        update_query_model(true);
    }
}

void MainWindow::on_action_2_triggered()
{
    // окно для просмотра взвешиваний по дням
    actibe_table_view = ui->tableView_2;
    work_date = cur_date;
    setDisabled(true);
    update_query_model(false);
    ui->stackedWidget->setCurrentIndex(1);
    work_index_widget = 1;
    ui->listWidget->clear();
    ui->listWidget->addItems(get_all_dates());
    if(ui->listWidget->count()) ui->listWidget->setCurrentRow(0);
    setDisabled(false);
}

void MainWindow::on_action_triggered()
{
    // окно для просмотра взыешиваний за текущие сутки
    actibe_table_view = ui->tableView;
    work_date = cur_date;
    setDisabled(true);
    update_query_model(false);
    ui->stackedWidget->setCurrentIndex(0);
    work_index_widget = 0;
    setDisabled(false);
}

void MainWindow::on_serial_fall_off(const QString &message)
{
    if(serial_timer->isActive()) serial_timer->stop();
    ui->label->setText(message);
    cur_weight = 0;
}

void MainWindow::on_serial_restart(bool show_message)
{
    if(serial_timer->isActive()) serial_timer->stop();
    ui->label->setText("0");
    if(serial.isOpen()) serial.close();
    serial_port_search();

    serial.setPortName(appdata->port_name);
    if(!serial.open(QIODevice::ReadOnly)){
        QMessageBox::critical(nullptr, "Ошибка",
                              "Невозможно открыть порт контроллера весов.\n"+
                              serial.errorString(),
                              QMessageBox::Close);
        on_serial_fall_off(serial_no_connect);
    }
    else{
        on_serial_fall_off("0");
        if(show_message) {
            QMessageBox::information(nullptr, "Порт активен",
                              serial.portName() + " активен.\n",
                              QMessageBox::Ok);
        }
    }

    if(serial.isOpen() && !serial_timer->isActive())
        serial_timer->start(serial_timer_delay);
}

void MainWindow::on_action_3_triggered()
{
    on_serial_restart(true);
}


void MainWindow::serial_port_search(){
    appdata->port_name.clear();
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    for(auto &port : ports){
        if(port.vendorIdentifier()  == appdata->vendor_id &&
                port.productIdentifier() == appdata->product_id)
        {
            if(!port.isBusy()){
                appdata->port_name = port.portName();
            }
            else{
                QMessageBox::critical(nullptr, "Ошибка",
                                      "Порт контроллера весов занят.",
                                      QMessageBox::Close);
            }
            break ;
        }
    }

    if(!ports.isEmpty() && appdata->port_name.isEmpty()){
        QStringList list;
        PortSelectionDialog dlg;
        for(auto &port : ports){
            list.append(port.portName());
        }
        dlg.set_list(list);
        if(dlg.exec() != QDialog::Rejected)
            appdata->port_name = dlg.get_selected_item();
    }
}

QStringList MainWindow::get_all_dates()
{
    if(!query.exec(select_all_dates)){
        QMessageBox::critical(nullptr, "Ошибка",
                              "Не возможно получить данные из base.\n" +
                              query.lastError().text(), QMessageBox::Close);
        exit(0);
    }
    QStringList list;
    query.first();
    do {
        if(query.isValid())
            list.push_back(query.value(0).toString());
    } while (query.next());

    return list;
}

void MainWindow::on_listWidget_clicked(const QModelIndex &index)
{
    // выборка по указанной дате
    if(!index.isValid()) return ;
    setDisabled(true);
    // отбор данных по дате
    //QString query_text = QString(select_cur_date).arg(index.data().toString());
    work_date = QDate::fromString(index.data().toString(), "dd.MM.yyyy");

    // Привязка модели
    update_query_model(false);
    setDisabled(false);
}

