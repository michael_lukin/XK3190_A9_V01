#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QDialog>
#include <QDateTime>
#include <QDoubleValidator>

#include "appdata.h"

class Data{ // глобальные данны приложения
public:
    enum Type{IN, OUT};

    Data(){clear();}

    void clear(){
        rowid    = 0;
        number   = 1; /// !!! автоинкремент
        datetime = QDateTime::currentDateTime();
        cargo    = "";
        weight   = 0.0;
        regnum   = "";
        type     = Data::IN;
        counteragent = "";
        info     = "";
    }

public:
    size_t    rowid;
    uint32_t  number;
    QDateTime datetime;
    QString   cargo;
    double    weight;
    QString   regnum;
    Type      type;
    QString   counteragent;
    QString   info;
};

namespace Ui {
class UpdateDialog;
}

class UpdateDialog : public QDialog
{
    Q_OBJECT
public:
    enum QueryType {UPDATE, INSERT};
public:
    explicit UpdateDialog(AppData &data, QWidget *parent = 0);
    ~UpdateDialog();
private:
    void showEvent(QShowEvent *event);
private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

//    void on_lineEdit_4_textEdited(const QString &arg1);

public:
    QueryType query_type = UPDATE;
    Data data;
private:
    Ui::UpdateDialog *ui;
    AppData *appdata;
};



#endif // UPDATEDIALOG_H
