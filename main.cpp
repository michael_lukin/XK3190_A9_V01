#include "mainwindow.h"
#include <QApplication>
#include <QSerialPortInfo>

#include "appdata.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/icon.png"));

    AppData appdata;
    if(!appdata.isValid() ) exit(0);

    MainWindow w(appdata);
    w.show();

    return a.exec();
}
