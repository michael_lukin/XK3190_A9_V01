#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QTextDocument>
#include <QTextDocumentWriter>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QPrinter>
#include <QDir>

#include <QTableView>

#include "appdata.h"
#include "updatedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(AppData &data, QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);
    void on_pushButton_clicked();           // добавление данных в базу
    void on_action_PDF_triggered();
    void on_action_PDF_2_triggered();
    void on_action_remove_cur_record_triggered();
    void on_action_edit_cur_record_triggered();
    void on_tableView_customContextMenuRequested(const QPoint &pos);
    void handleReadyRead();
    void handleError(QSerialPort::SerialPortError error);
    void on_action_triggered();   // переход к окну взвешивания за сутки
    void on_action_2_triggered(); // переход к окну взвешивания по дням
    void on_action_3_triggered(); // вызов serial restart
    // селект даты из списка
    void on_listWidget_clicked(const QModelIndex &index);


private:
    void on_time_out();
    void on_dwatch_time_out();
    void on_serial_fall_off(const QString &);
    void on_serial_restart(bool show_message);
    void serial_port_search();
    QStringList get_all_dates();
    void update_query_model(bool main_counter_recal);

private:
    Ui::MainWindow *ui;

private:
    UpdateDialog update_dlg;
    int serial_timer_delay   = 1000; // таймаут для ожидания данных порта
    size_t work_index_widget = 0;    // рабочая закладка в stackedWidget
    QDate  work_date;                // дата для имени отчета
    AppData *appdata;
    QMenu *table_menu;               // контекстное меню
    QSqlQuery query;                 // один объект query на все зопросы
    QSqlQueryModel query_model;      // модель для tableView и tableView_2
    QTableView *actibe_table_view = nullptr;
    QPrinter printer;
    QSerialPort serial;
    QTimer *serial_timer;            // таймер для ожидания данных порта
    QDate cur_date;                  // поле для отслеживания смены суток
    QTimer date_watch_timer;         // тамер отслеживающий смену суток
/// для разбора данных
    double  cur_weight = 0.0;        // последнее считанное значение веса
    bool   begin   = false;          // признак начала пакета данных
    size_t count   = 0;              // счетчик обработанных байтов
    size_t dot_num = 0;              // позиция точки в полученных данных
    char res[7];                     // байты хранящее полученное значение веса
};

#endif // MAINWINDOW_H
