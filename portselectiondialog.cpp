#include "portselectiondialog.h"
#include "ui_portselectiondialog.h"

PortSelectionDialog::PortSelectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PortSelectionDialog)
{
    setWindowFlags(Qt::WindowSystemMenuHint/* |
                   Qt::WindowCloseButtonHint */);
    ui->setupUi(this);
}

PortSelectionDialog::~PortSelectionDialog()
{
    delete ui;
}

QString PortSelectionDialog::get_selected_item(){
    return ui->comboBox->currentText();
}

void PortSelectionDialog::set_list(const QStringList &list){
    ui->comboBox->clear();
    ui->comboBox->addItems(list);
}

void PortSelectionDialog::on_buttonBox_accepted()
{
    accept();
}

void PortSelectionDialog::on_buttonBox_rejected()
{
    reject();
}
