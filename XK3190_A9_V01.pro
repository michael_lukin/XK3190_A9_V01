#-------------------------------------------------
#
# Project created by QtCreator 2016-11-08T22:26:46
#
#-------------------------------------------------

QT       += core gui sql printsupport serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = XK3190_A9_V01
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    appdata.cpp \
    updatedialog.cpp \
    portselectiondialog.cpp

HEADERS  += mainwindow.h \
    appdata.h \
    updatedialog.h \
    portselectiondialog.h

FORMS    += mainwindow.ui \
    updatedialog.ui \
    portselectiondialog.ui

DISTFILES += \
    readme.txt

RESOURCES += \
    src.qrc

RC_ICONS = icon.ico
RC_FILE = myapp.rc
