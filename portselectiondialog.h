#ifndef PORTSELECTIONDIALOG_H
#define PORTSELECTIONDIALOG_H

#include <QDialog>

namespace Ui {
class PortSelectionDialog;
}

class PortSelectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PortSelectionDialog(QWidget *parent = 0);
    ~PortSelectionDialog();

    QString get_selected_item();
    void set_list(const QStringList &list);

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::PortSelectionDialog *ui;
};

#endif // PORTSELECTIONDIALOG_H
