#include "updatedialog.h"
#include "ui_updatedialog.h"

UpdateDialog::UpdateDialog(AppData &data, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog),
    appdata(&data)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowSystemMenuHint |
                   Qt::WindowCloseButtonHint );

//    QDoubleValidator *doubleVal = new QDoubleValidator(-999999.0, +999999.0, 4, this );
//    ui->weight_edit->setValidator( doubleVal );
//    doubleVal->setLocale(QLocale::c());
    ui->weight_edit->setMaxLength(12);
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}

void UpdateDialog::showEvent(QShowEvent *event)
{
    QDialog::showEvent(event);
    ui->number_edit->setValue(data.number);
    ui->datetime_edit->setDateTime(data.datetime);
    ui->cargo_edit->setCurrentText(data.cargo);
    ui->weight_edit->setText(QString::number(data.weight));
    ui->regnum_edit->setText(data.regnum);
    ui->type_edit->setCurrentIndex((data.type == Data::IN)? 0 : 1);
    ui->note_edit->setText(data.info);
    ui->counteragent_edit->setText(data.counteragent);

    if(query_type == UPDATE){
        ui->type_edit->setDisabled(true);
    }
    else{
        ui->type_edit->setEnabled(true);
    }

}

void UpdateDialog::on_buttonBox_accepted()
{
    QString join_key;
    // если производится запись выездного взвешивания
    if(query_type == INSERT &&
       ui->type_edit->currentText() == "Выездное")
    {
        // получить запись въезда этой же машины
        QSqlQuery select_query(appdata->d_base);
        QString query_text = QString(select_l_coming)
                             .arg(ui->datetime_edit->dateTime().toString("dd.MM.yyyy"))
                             .arg(ui->regnum_edit->text());

        if(!select_query.exec(query_text)){
            QMessageBox::critical(nullptr, "Ошибка",
                                  "Ошибка запроса на получение данных из БД\n" +
                                  select_query.lastError().text(),
                                  QMessageBox::Close);
            done(-1);
        }
        select_query.first();
        // получить значение поля time_stamp
        join_key = select_query.value(0).toString();
//        qDebug() << query_text.toStdString().c_str();
    }
    else if(query_type == INSERT){
        join_key = ui->datetime_edit->dateTime().toString("yyyy-MM-dd HH:mm:ss");
    }

    QSqlQuery query(appdata->d_base);
    QString query_text;
    if(query_type == INSERT){
        query_text = QString(insert_new_data)
        .arg(ui->number_edit->text())
        .arg(ui->datetime_edit->dateTime().toString("yyyy-MM-dd HH:mm:ss"))
        .arg(ui->cargo_edit->currentText())
        .arg(ui->weight_edit->text().toDouble())
        .arg(ui->regnum_edit->text())
        .arg(ui->type_edit->currentText())
        .arg(ui->counteragent_edit->text())
        .arg(ui->note_edit->text())
        .arg(join_key);
    }
    else{
        query_text = QString(update_cur_row)
        .arg(ui->cargo_edit->currentText())
        .arg(ui->regnum_edit->text())
        .arg(ui->type_edit->currentText())
        .arg(ui->counteragent_edit->text())
        .arg(ui->note_edit->text())
        .arg(QString::number(data.rowid));
    }
//    qDebug() << query_text.toStdString().c_str();
    if(!query.exec(query_text)){
        QMessageBox::critical(nullptr, "Ошибка",
                              "Ошибка запроса на запись данных в БД\n" +
                              query.lastError().text(), QMessageBox::Close);
        done(-1);
    }
    accept();
}

void UpdateDialog::on_buttonBox_rejected()
{
    reject();
}

//void UpdateDialog::on_lineEdit_4_textEdited(const QString &arg1)
//{
//    int pos = arg1.indexOf(',');
//    if(pos != -1){
//        QString res = arg1;
//        res[pos] = '.';
//        ui->weight_edit->setText(res);
//    }
//}
