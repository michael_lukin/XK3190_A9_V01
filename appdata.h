#ifndef APPDATA_H
#define APPDATA_H

/// ТЕКСТ ЗАПРОСОВ
//const char order_by_regnum[] = R"(select number, strftime("%d.%m.%Y %H:%M:%S", time_stamp, "localtime") as _timestamp, cargo, weight, reg_num, type, note, rowid from  main_table where strftime("%d.%m.%Y", time_stamp, 'localtime') = "%1" order by  reg_num,  time_stamp;)";
//const char update_cur_row [] = R"(update main_table  set number = "%1", time_stamp = datetime("%2", "utc"), cargo = "%3", weight = "%4", reg_num = "%5", type = "%6", counteragent = "%7" note = "%8" where rowid = "%8";)";
//const char select_cur_date[] = R"(select number, strftime("%d.%m.%Y %H:%M:%S", time_stamp, "localtime") as _timestamp, cargo, weight, reg_num, type, counteragent, note, rowid from  main_table where date(time_stamp, "localtime") == date('now', "localtime");)";
const char select_cur_date [] = R"(select number, strftime("%d.%m.%Y %H:%M:%S", time_stamp, "localtime") as _timestamp, cargo, weight, reg_num, type, counteragent, note, rowid from  main_table where  strftime("%d.%m.%Y", time_stamp, 'localtime') = "%1" order by time_stamp;)";
const char select_l_coming [] = R"(select datetime(max(time_stamp),"localtime") as _key from  main_table where strftime("%d.%m.%Y", time_stamp, 'localtime') = "%1" and type = "Въездное" and  reg_num = "%2";)";
const char select_for_pdf2 [] = R"(select number, strftime("%d.%m.%Y %H:%M:%S", time_stamp, "localtime") as _timestamp, cargo, weight, reg_num, type, counteragent, note, join_key from  main_table where strftime("%d.%m.%Y", time_stamp, 'localtime') = "%1" order by join_key, type;)";
const char select_all_dates[] = R"(select distinct strftime("%d.%m.%Y", time_stamp,'localtime') as _date from main_table  order by time_stamp desc;)";
const char select_max_num  [] = R"(select max(number) as number from  main_table where date(time_stamp, "localtime") == date('now', "localtime");)";
const char insert_new_data [] = R"(insert into main_table ( number, time_stamp, cargo, weight, reg_num, type, counteragent, note, join_key) values ( "%1", datetime("%2", "utc") , "%3", "%4", "%5", "%6", "%7", "%8", datetime("%9", "utc") );)";
const char update_cur_row  [] = R"(update main_table  set cargo = "%1", reg_num = "%2", type = "%3", counteragent = "%4" note = "%5" where rowid = "%6";)";
const char remove_cur_row  [] = R"(delete from  main_table where rowid = "%1";)";

const char serial_no_data[] = R"(Нет данных)";
const char serial_no_connect[] = R"(Отсутствует подключение)";

#include <QDir>
#include <QFile>
#include <QtSql>
#include <QMessageBox>

#include "portselectiondialog.h"

class AppData{
public:
    AppData();
    ~AppData(){}
    bool isValid(){return is_valid;}
    uint32_t max_num_for_cur_date();

private:
    bool baudRateIsValid();

public:
    // база взвешиваний
    QSqlDatabase d_base;
    // счетчик кол-ва взвешиваний за сутки
    uint32_t main_counter = 0;
    // данные для поиска COM порта контроллера
    int product_id;
    int vendor_id;
    // данные для подключения к порту
    int baud_rate;
    QString port_name;

private:
    QSqlDatabase config;
    bool is_valid;
};

#endif // APPDATA_H
