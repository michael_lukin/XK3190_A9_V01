#include "appdata.h"

AppData::AppData() : is_valid(true) {
    QDir dir;
    QFile file;
    d_base = QSqlDatabase::addDatabase("QSQLITE", "d_base");
    config = QSqlDatabase::addDatabase("QSQLITE", "config");

    config.setDatabaseName("config");
    d_base.setDatabaseName("dbase/base");

    // проверка файла конфигурации
    if(!file.exists("config") || !config.open()){
        is_valid = false;
        QMessageBox::critical(nullptr, "Ошибка",
                              "Невозможно открыть конфигурационный файл",
                              QMessageBox::Close);
        /// ОТПРАВИТЬ ДАННЫЕ В LOG !!!
        return ;
    }
    // проверка служебных каталогово
    if(!dir.exists("dbase") ){
        if(!dir.mkdir("dbase") ){
            is_valid = false;
            QMessageBox::critical(nullptr, "Ошибка",
                                  "Невозможно создать каталог dbase",
                                  QMessageBox::Close);
            /// ОТПРАВИТЬ ДАННЫЕ В LOG !!!
            return ;
        }
    }
    // проверка основной базы
    if(!d_base.open()){
        is_valid = false;
        QMessageBox::critical(nullptr, "Ошибка",
                              "Невозможно открыть базу данных base",
                              QMessageBox::Close);
        /// ОТПРАВИТЬ ДАННЫЕ В LOG !!!
        return ;
    }
    // проверка таблицы в d_base
    QSqlQuery d_base_query(d_base);
    // d_base_query = std::move(QSqlQuery(d_base));

    if(!d_base_query.exec("select * from main_table where rowid = 1;")){
        is_valid = false;
        QMessageBox::critical(nullptr, "Ошибка",
                              "База данных base повреждена",
                              QMessageBox::Close);
        /// ОТПРАВИТЬ ДАННЫЕ В LOG !!!
        return ;
    }

    // получить значение счетчика взвешиваний на текущий день
    main_counter = max_num_for_cur_date() + 1;

    // загрузка конфигурационных данных SERIAL порта
    QSqlQuery query_to_conf(config);
    if(!query_to_conf.exec("select * from main_table")){
        is_valid = false;
        QMessageBox::critical(nullptr, "Ошибка",
                              "База данных config повреждена",
                              QMessageBox::Close);
        /// ОТПРАВИТЬ ДАННЫЕ В LOG !!!
        return ;
    }
    // считывание парметров SERIAL порта
    query_to_conf.first();
    product_id = query_to_conf.value("product_id").toInt();
    vendor_id  = query_to_conf.value("vendor_id" ).toInt();
    baud_rate  = query_to_conf.value("baud_rate" ).toInt();
    if(!baudRateIsValid() ) baud_rate = 9600;
    /// может сюда стоит задать инициализацию для SERIALPORT???

    // закрытие конфигурационного файла
    config.close();
}

uint32_t AppData::max_num_for_cur_date(){
    if(!is_valid) return 0;
    QSqlQuery query(d_base);
    if(!query.exec(select_max_num)){
        QMessageBox::critical(nullptr, "Ошибка",
                              "Невозможно выполнить запрос к base",
                              QMessageBox::Close);
        is_valid = false;
        return 0;
    }
    query.first();
    return query.value(0).toInt();
}

bool AppData::baudRateIsValid(){
    return baud_rate == 600  || baud_rate == 1200 ||
            baud_rate == 2400 || baud_rate == 4800 || baud_rate == 9600;
}
